<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
	function doChangePage(pageno){
		//当如下语句执行的时候就相当于点击了超级链接
		document.location.href="${pageContext.request.contextPath}/servlet/${param.module}Query?pageno="+pageno;
	}
</script>


<div>
		<input type="button" value="首页" onclick="doChangePage(1)" ${pageModel.pageno==1?"disabled":""}>
		<input type="button" value="上一页" onclick="doChangePage(${pageModel.pageno-1})" ${pageModel.pageno==1?"disabled":""}>
		<input type="button" value="下一页" onclick="doChangePage(${pageModel.pageno+1})" ${pageModel.pageno==pageModel.totalPage?"disabled":""}>
		<input type="button" value="尾页" onclick="doChangePage(${pageModel.totalPage})"  ${pageModel.pageno==pageModel.totalPage?"disabled":""}>
		<input type="hidden" name="cp">
		<span style="color: red"> ${pageModel.pageno} / ${pageModel.totalPage} </span> 跳转到：
		<select name="selPage" onchange="doChangePage(this.value)">
			<c:forEach var="i" begin="1" end="${pageModel.totalPage}">
				<option value="${i }"  ${pageModel.pageno==i?"selected":"" }>
					${i }
				</option>
			</c:forEach>
		</select>
</div>
