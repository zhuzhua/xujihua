<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<title>Insert title here</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
		<script>
			function doDelete(id){
				if(confirm("确定要删除吗")){
					document.location.href="${pageContext.request.contextPath}/servlet/examDelete?id="+id+"&pageno=${pageModel.pageno}";
				}
			}
			
		</script>

	</head>
	<body style="overflow: scroll; overflow-y: hidden">
		<div>

			<div id="ks">
				<!--ks_top-->
				<div class="main_top">
					<p>
						试题管理
					</p>
				</div>
				<!--ks_bottom-->
				<div class="ks_bottom">

					<!--ti-->
					<div class="ti">
						
						<c:forEach var="exam" items="${pageModel.model }" varStatus="num">
						
						
						
						<p>
							<span style="font-weight: bold; color: #296DB8;">${(pageModel.pageno-1)*pageModel.pageRecord+num.count}、${exam.title }</span>

						</p>

						<p>
							<div id="div${num.count }" align="left" style="padding-left: 36px">
								<span name="option" style="font-weight: bold;">A、${exam.optionA }</span>
								<span name="option"
									style="font-weight: bold; padding-left: 120px">B、${exam.optionB}</span>
								<span name="option"
									style="font-weight: bold; padding-left: 120px">C、${exam.optionC }</span>
								<span name="option"
									style="font-weight: bold; padding-left: 120px">D、${exam.optionD}</span>

							</div>
						</p>
						<p style="padding-left: 36px">
							(答案是:&nbsp;
							<span id="an3" style="font-weight: bold; color: #F80015;">${exam.answer }</span>&nbsp;)&nbsp;&nbsp;&nbsp;[

							<a href="${pageContext.request.contextPath }/servlet/examEditUI?id=${exam.id}"><span
								style="color: #FF8005;">编辑</span>
							</a>&nbsp;&nbsp;
							<a href = "#" onclick="doDelete(${exam.id})"><span
								style="color: #FF8005;">删除</span>
							</a>]
						</p>
						
					<script>
						//拿到答案
						var answer = "${exam.answer}";
						var divObj = document.getElementById("div${num.count}");
						//通过标签名称获得标签对象数组（拿到选项数组）
						var optionArray = divObj.getElementsByTagName("span");
						
						for(var i = 0;i < optionArray.length;i++){
							//获得标签对中的文本
							var innerText = optionArray[i].innerHTML;
							if(answer==innerText.substring(0,1)){
								optionArray[i].style.color="red";
							}
						}
						
						
					</script>
						
						
						
						</c:forEach>

					</div>
					
					<%--<%@include file="/page.jsp" %> --%>
				<jsp:include page="/page.jsp">
					<jsp:param name="module" value="exam" />
				</jsp:include>
				</div>

			</div>

		</div>
		</div>
		</div>
	</body>
</html>
