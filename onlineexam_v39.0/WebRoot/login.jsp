<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>My JSP 'login.jsp' starting page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="${pageContext.request.contextPath }/style/login.css" rel="stylesheet" type="text/css">
	</head>
	<script type="text/javascript">
		//页面加载完毕后会默认执行onload方法
		window.onload=function (){
			if(window.self!=window.top){//当前的页面不是在顶层显示，那么就去顶层显示
				window.top.location.href = "${pageContext.request.contextPath}/login.jsp";
			}
		}
	
	</script>


	<body>
		<div id="login">

			<!--bj-->
			<div class="bj">
				<!--left-->
				<div class="bj_left">
					<img src="${pageContext.request.contextPath }/images/login_left.jpg" />
				</div>
				<!--right-->
				<div class="bj_right">
					<!--rb-->

					<div align="left" class="rb">

						<form action="${pageContext.request.contextPath }/servlet/login"
							name="uform" method="post">
							<font color="red">${errorInfo }
							</font>
							<p>
							用户代码：
								<input id="ucode" name="ucode" type="text" value="${param.ucode }"
									style="background: none; border: none; background: #FFFFFF; border: solid 1px #8AB0BB; text-align: left; width: 160px; height: 20px; padding-left: 5px; line-height: 20px;" />
								<font color="red"><span id="uerror"></span>
								</font>

							</p>
							<br>
							<p>
								用户名：&nbsp&nbsp
								<input id="username" name="uname" type="text" value="${param.uname }"
									style="background: none; border: none; background: #FFFFFF; border: solid 1px #8AB0BB; text-align: left; width: 160px; height: 20px; padding-left: 5px; line-height: 20px;" />
								<font color="red"><span id="uerror"></span>
								</font>

							</p>

							<br>
							<p>
								密&nbsp;&nbsp;码：&nbsp&nbsp
								<input name="pwd" type="password" value="${param.pwd }"
									style="background: none; border: none; background: #FFFFFF; border: solid 1px #8AB0BB; text-align: left; width: 160px; height: 20px; padding-left: 5px; line-height: 20px;" />
							</p>
							
							<p style="margin-top: 10px;">
								
								
								<input name="" type="reset"
									value="重&nbsp;&nbsp;置"
									style="cursor:pointer;background: none; border: none; width: 70px; height: 23px; background: url(${pageContext.request.contextPath }/images/button.jpg) no-repeat; color: #FFFFFF; margin-left: 5px; font-size: 14px;" />
									&nbsp;&nbsp;&nbsp;&nbsp;		&nbsp;&nbsp;&nbsp;&nbsp;
									<input name="username" type="submit" value="确&nbsp;&nbsp;认"
									style="cursor:pointer;background: none; border: none; width: 70px; height: 23px; background: url(${pageContext.request.contextPath }/images/button.jpg) no-repeat; color: #FFFFFF; font-size: 14px;" />
							</p>

						</form>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
