
function trim(str){
	//创建空格对象
	var space = new String(" ");
	/*
	str = trimLeft(str,space);
	str = trimRight(str,space);
	*/
	return trimRight( trimLeft(str,space),space);
}

//去除左空格
function trimLeft(str,space){
	
	var i = 0,j = str.length;
	while(i < j && space.indexOf(str.charAt(i))!=-1){
		i++;
	}
	
	return  str.substring(i,str.length);	
	
}

//去除右空格
function trimRight(str,space){
	
	var i = 0,j = str.length;
	while(j > i && space.indexOf(str.charAt(j-1))!=-1){
		j--;
	}
	return  str.substring(i,j);	
}