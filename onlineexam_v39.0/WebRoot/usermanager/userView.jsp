<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.bjpowernode.web.usermanager.bean.*,java.util.*" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/zxy/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<title>Insert title here</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		function deleteUser(){
			if(confirm("确定删除")){
				
			}
		}
		//通过父选项选择所有子选项，或者取消所有的子选项
		function checkORcancelAll(){
			//获得父选项对象
			var parentCBoxObj = document.getElementById("parentCBox");
			//获得子选项对象数组
			var childCBoxArray = document.getElementsByName("childCBox");
			//通过父选项选择所有子选项，或者取消所有的子选项
			if(parentCBoxObj.checked){
				for(var i = 0;i < childCBoxArray.length;i++){
					childCBoxArray[i].checked=true;
				}
			}else{
				for(var i = 0;i < childCBoxArray.length;i++){
					childCBoxArray[i].checked=false;
				}
			}
			checkOrcancel();
		}
		
		//选项与按钮的联动关系
		function checkOrcancel(){
			
			//获得父选项对象
			var parentCBoxObj = document.getElementById("parentCBox");
			//获得子选项对象数组
			var childCBoxArray = document.getElementsByName("childCBox");
			//获得修改对象
			var updateBtnObj = document.getElementById("updateBtn");
			//获得删除对象
			var deleteBtnObj = document.getElementById("deleteBtn");
			//统计子选项选中的个数
			var count = 0;
			for(var i = 0;i < childCBoxArray.length;i++){
				if(childCBoxArray[i].checked){
					count ++;
				}
			}
			
			if(count == 0){ //一个未选择 修改和删除都不可以使用
				updateBtnObj.disabled=true;
				deleteBtnObj.disabled=true;
			}else if(count == 1){//选择一个 修改和删除都可以使用
				updateBtnObj.disabled=false;
				deleteBtnObj.disabled=false;
			}else if(count > 1){//选择一个以上 修改不可用，删除可以使用
				updateBtnObj.disabled=true;
				deleteBtnObj.disabled=false;
			}
			//子选项全选择，选中父选项，否则取消父选项
			if(count == childCBoxArray.length){
				parentCBoxObj.checked=true;
			}else{
				parentCBoxObj.checked=false;
			}
		}
		
		var oldColor;
		//用户将鼠标指针移动到对象内时触发。
		function doMouseOver(tr){
			//存储原来的颜色
			oldColor = tr.style.background;
			//改变当前对象的背景颜色
			tr.style.background="#ADADAD";
		}
		//用户将鼠标指针移出对象边界时触发。
		function doMouseOut(tr){
			tr.style.background=oldColor;
		}
		//双击选中行或者取消行
		function checkORcancelTr(tr){
			//拿到第一个单元格(第一个列)
			var cellObj = tr.cells[0];			
			//第一个单元格中的input对象  [getElementsByTagName 通过标签名称获得标签对象的集合]
			var inputObj = cellObj.getElementsByTagName("input")[0];
			if(inputObj.checked){
				inputObj.checked = false;
			}else{
				inputObj.checked = true;
			}
			checkOrcancel();
		}
		
		function doEditUI(){
			//提交form表单
			document.forms["uform"].submit();
		}
		function doDelete(){
			if(confirm("确定需要删除吗？")){
				//获得表单对象
				var formObj = document.forms["uform"];
				formObj.action="${pageContext.request.contextPath}/servlet/userDelete";
				formObj.submit();
			}
			
		}
	</script>
	</head>
	<!-- 去除纵向滚动条  style="overflow:scroll;overflow-y:hidden" -->
	<body style="overflow: scroll; overflow-y: hidden">
		<div>


			<div id="main">
			<!--main_top-->
				<div class="user_manager">
					<p>
						用户管理
						<span style="float: right;margin-right: 20px">
						<input type="button" value="添加"    onclick="document.location='${pageContext.request.contextPath}/usermanager/userAdd.jsp'" style="width: 54px; height: 20px; margin-left: 15px;">
						<input type="button" value="修改" id="updateBtn" disabled   onclick="doEditUI()"  style="width: 54px; height: 20px; margin-left: 15px;">
						<input type="button" value="删除" id="deleteBtn" disabled onclick="doDelete()" style="width: 54px; height: 20px; margin-left: 15px;">
						</span>
					</p>
				</div>
				<!--main_bottom-->

				<div class="main_bottom">
				  <form name="uform" action="${pageContext.request.contextPath}/servlet/userEditUI" >
					<input type="hidden" name="pageno" value="${pageModel.pageno }">
					<table width="100%" class="table" cellspacing="1"
						style="background: #BAC2CF; font-size: 14px;">
						<tr
							style="background: #F6F6F6; border: 0; text-align: center; line-height: 25px;">
							<td width="6%" height="26" background="../images/tab_14.gif" class="STYLE1">
							<div align="center" class="STYLE2 STYLE1">
							<input type="checkbox" id="parentCBox" value="checkbox" onclick="checkORcancelAll()" /></div></td>
							
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								编号
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								用户代码
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								用户名称
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								邮箱
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								电话
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								时间
							</td>


						</tr>

					    <c:forEach var="user" items="${pageModel.model }" varStatus="num">
					    			<tr
					    	onmouseover="doMouseOver(this)"
					    	onmouseout="doMouseOut(this)"
					    	ondblclick="checkORcancelTr(this)"
							style="background: #FFFFFF; border: 0; text-align: center; line-height: 25px;">
							<td height="18" ><div align="center" class="STYLE1">
              <input name="childCBox" type="checkbox" class="STYLE2" value="${user.id }"
               onclick="checkOrcancel()"
               />
            </div></td>
							
							<td style="color: #666666; font-size: 14px;">
								${(pageModel.pageno-1)*pageModel.pageRecord+num.count}
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${user.ucode}
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${user.uname}
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${user.email}
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${user.telephone}
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${fn:subString(user.registerTime, 0,10)}
							</td>
						</tr>
						</c:forEach>

					</table>
					
					</form>
					
				<%--<%@include file="/page.jsp" %> --%>
				<jsp:include page="/page.jsp">
					<jsp:param name="module" value="user" />
				</jsp:include>

				</div>
			</div>


		</div>
		</div>
		</div>
	</body>
</html>
