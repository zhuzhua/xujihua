<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<title>Insert title here</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/exam.js"></script>
	<script type="text/javascript">
			function doSave(){
				//获得form对象
				var formObj = document.forms["uform"];
				var flag = validate(formObj);
				if(flag){
					//提交请求[如下语句执行就相当于点击了提交按钮]
					formObj.submit();
				}
			}
			
			//非空校验
			function validate(formObj){
				
				//获得ucode对象
				var ucodeObj = formObj.ucode;
				if(trim(ucodeObj.value).length <= 0){
					alert("用户代码不可以为空！");
					ucodeObj.value="";
					ucodeObj.focus();
					return false;
				}
				var unameObj = formObj.uname;
				if(trim(unameObj.value).length <= 0){
					alert("用户名称不可以为空！");
					unameObj.value="";
					unameObj.focus();
					return false;
				}
				var pwdObj = formObj.pwd;
				if(trim(pwdObj.value).length <= 0){
					alert("密码不可以为空！");
					pwdObj.value="";
					pwdObj.focus();
					return false;
				}
				//确认密码
				var checkpwdObj = formObj.checkpwd;
				if(trim(pwdObj.value)!=checkpwdObj.value){
					alert("两次密码输入不一致！");
					checkpwdObj.value="";
					pwdObj.value="";
					pwdObj.focus();
					return false;
				}
				var emailObj = formObj.email;
				if(trim(emailObj.value).length <= 0){
					alert("邮箱不可以为空！");
					emailObj.value="";
					emailObj.focus();
					return false;
				}
				var telephoneObj = formObj.telephone;
				if(trim(telephoneObj.value).length <= 0){
					alert("电话不可以为空！");
					telephoneObj.value="";
					telephoneObj.focus();
					return false;
				}
				return true;
				
				
			}
			
			var xmlHttpReqeust;
			function checkUcode(ucode){
				//1:创建ajax核心对象
				createXMLHttpRequest();
				//2:注册回调函数
				xmlHttpReqeust.onreadystatechange=callback;
				//3:设置请求参数
				var url = "${pageContext.request.contextPath}/servlet/checkUcode?ucode="+ucode.value+"&ts="+new Date().getTime();
				xmlHttpReqeust.open("get",url);
				//4:发送请求
				xmlHttpReqeust.send();
				
			}
			//1:创建ajax核心对象
			function createXMLHttpRequest(){
				if(window.XMLHttpRequest){
					xmlHttpReqeust = new XMLHttpRequest();
				}else if(window.ActiveXObject){
					xmlHttpReqeust = new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			//2:注册回调函数
		   function callback(){
			   if(xmlHttpReqeust.readyState==4){
				   if(xmlHttpReqeust.status==200){
					   //获得响应的文本
					   var resInfo = xmlHttpReqeust.responseText;
					   //获得span对象
					   var spanObj = document.getElementById("info");
					   //把文本插入到span标签对中
					   spanObj.innerHTML=resInfo;
				   }else if(xmlHttpReqeust==404){
					   alert("==404==");
				   }else if(xmlHttpReqeust==405){
					   alert("==405==");
				   }else if(xmlHttpReqeust==500){
					   alert("==500==");
				   }else{
					   alert("==未知的异常==");
				   }
			   }
		   }
	</script>
	</head>

	<body >
		<div class="right_f">


			<!--right-->
			<div id="right">
				<!--right_top-->
				<div class="right_top">
					<p>
						用户信息
					</p>
				</div>

				<div class="right_bottom">
					<!--baise-->
					<div class="baise">
						<form name="uform" action="${pageContext.request.contextPath}/servlet/userInsert" method="post">
							<table width="100%" class="table" cellspacing="1"
								style="background: #FFFFFF; font-size: 12px; text-align: center; margin: 0 auto; border: 0;">
								<tr style="background: #FFFFFF; border: 0;">
									<td height="20" align="right" width="46%">
										用户代码：
									</td>
									<td width="54%" style="text-align: left; padding-left: 10px;">
										<input id="ucode" name="ucode" type="text" maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" onblur="checkUcode(this)" />

										<span id="info" style="color: red">*</span>
									
									</td>

								</tr>
								<tr style="background: #FFFFFF; border: 0;">
									<td height="20" align="right" width="46%">
										用户名称：
									</td>
									<td width="54%" style="text-align: left; padding-left: 10px;">
										<input id="uname" name="uname" type="text" maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" />

										<span id="responseInfo" style="color: red"></span>
										<font color='red'>*</font>
									</td>

								</tr>
								<tr
									style="background: #FFFFFF; border: 0; margin-top: 10px; height: 25px;">
									<td style="width: ;" align="right">
										密码：
									</td>
									<td style="text-align: left; padding-left: 10px;">
										<input id="pwd" name="pwd" type="text" maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" />
										<font color='red'>*</font>
									</td>

								</tr>


								<tr
									style="background: #FFFFFF; border: 0; margin-top: 10px; height: 25px;">
									<td style="width: ;" align="right">
										密码确认：
									</td>
									<td style="text-align: left; padding-left: 10px;">
										<input id="checkpwd" name="checkpwd" type="text"
											maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" />
										<font color='red'>*</font>
									</td>
								</tr>

								<tr
									style="background: #FFFFFF; border: 0; margin-top: 10px; height: 25px;">

									<td align="right">
										邮箱：
									</td>
									<td style="text-align: left; padding-left: 10px;">
										<input id="email" name="email" type="text" maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" />
										<font color='red'>*</font>
									</td>
								</tr>

								<tr
									style="background: #FFFFFF; border: 0; margin-top: 10px; height: 25px;">
									<td align="right">

										电话：
									</td>

									<td style="text-align: left; padding-left: 10px;">
										<input id="telephone" name="telephone" type="text"
											maxlength="20"
											style="width: 165px; padding-left: 5px; border: none; border: solid 1px #C4D0CC;"
											value="" />
										<font color='red'>*</font>
									</td>
								</tr>
								<tr
									style="background: #FFFFFF; border: 0; margin-top: 10px; height: 25px;">
									<td align="right">

									</td>
									<td style="text-align: left; padding-left: 10px;">
										<input name="" value="重新填写" type="button"
											style="cursor:pointer; background: none; border: 0; width: 54px; height: 20px; background-color: #B6DDFC;" />
										<input name="" value="提交信息" type="button"
											onclick="doSave()"
											style="cursor:pointer; background: none; border: 0; width: 54px; height: 20px; background-color: #B6DDFC; margin-left: 15px;" />


									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>

		</div>
		</div>
		</div>
	</body>
</html>