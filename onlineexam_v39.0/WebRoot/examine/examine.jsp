<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/zxy/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<title>Insert title here</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
	</head>
	<body style="overflow: scroll; overflow-y: hidden">
		<div>
			<!--ks 59616911-->
			<script>

				window.onload = countTime;
				var timeLeft = 1 * 60 * 1000;
				function countTime() {
					var startMinutes = parseInt(timeLeft / (60 * 1000), 10);
					var startSec = parseInt((timeLeft - startMinutes * 60 * 1000) / 1000);
					document.getElementById('time').innerHTML = "剩余时间： " + startMinutes + "分钟 "
							+ startSec + "秒 ";
					timeLeft = timeLeft - 1000;
					var t = setTimeout('countTime() ', 1000);
				
					if (timeLeft == 0) {
						alert("时间到，点击交卷");
						document.getElementsByName("uform")[0].submit();
						closeExamine();
					}
				}
				
				//禁用F5(键盘点击事件)
				onkeydown=function (e){
					e = e||event;//浏览器的兼容写法
					if(e.keyCode==116){
						//alert("不好意思，F5禁用了。");
						return false;
					}
				}
				//禁用右键的
				oncontextmenu = function (){
					//alert("不好意思，右键禁用了。");
					return false;
				}
				//交卷
				function submitPaper(){
					document.forms["uform"].submit();
					closeExamine();
				}
				function closeExamine(){
					window.close();
				}
			</script>
			<div id="ks">
				<!--ks_top-->
				<div class="ks_top">
					<p>
						考试
						<span id='time' style="color: red; padding-left: 600px"></span>
					</p>

				</div>


				<!--ks_bottom-->
				<div class="ks_bottom">
					<div class="ti" style="overflow: auto;height: 460px"
						>
						<form name="uform" 
							action="${pageContext.request.contextPath}/servlet/calculateScore" method="post"
							target="content"
							>

							<input type="hidden" name="itemSize" value="${fn:size(exams)}">
						 <c:forEach var="exam" items="${exams }" varStatus="num">
								<p>
									<span style="font-weight: bold; color: #296DB8;">${num.count }、</span>${exam.title }
								</p>
								<input type="hidden" name="id${num.count }" value="${exam.id }">
								<p>
	
	
									<div align="left" id="div${num.count }"
										style="padding-left: 31px; font-weight: bold;">
										<input onclick="changeRadio(this,${num.count })" type="radio" name="option${num.count }"
											value="A" id="a${num.count }">
											<label for="a${num.count }"><span > A、${exam.optionA}</span></label>
										<br>
										<br>
										<input onclick="changeRadio(this,${num.count })" type="radio" name="option${num.count }"
											value="B" id="b${num.count }">
											<label for="b${num.count }"><span> B、${exam.optionB }</span></label>
	
										<br>
										<br>
										<input onclick="changeRadio(this,${num.count })" type="radio" name="option${num.count }"
											value="C" id="c${num.count }">
											<label for="c${num.count }"><span> C、${exam.optionC }</span></label>
										<br>
										<br>
										<input onclick="changeRadio(this,${num.count })" type="radio" name="option${num.count }"
											value="D" id="d${num.count }">
											<label for="d${num.count }"><span> D、${exam.optionD }</span></label>
									</div>
								</p>
							</c:forEach>
							<script type="text/javascript">
								function changeRadio(inputObj,count){
									//获得选中选项的值
									var inputValue = inputObj.value;
									//获得div对象
									var divObj = document.getElementById("div"+count);
									//获得div下的span数组(通过标签获得指定标签的数组)
									var spanArray = divObj.getElementsByTagName("span");
									for(var i = 0;i < spanArray.length;i++){
										var option = spanArray[i].innerHTML.substring(1,2);
										if(inputValue==option){
											spanArray[i].style.color="red";
										}else{
											spanArray[i].style.color="";
										}
									}
									//alert("abc".substring(1,2));
									
								}
							
							</script>
							<input type="button" value="交     卷" onclick="submitPaper()" >
						</form>
					</div>

				</div>

			</div>

		</div>
		</div>
		</div>
	</body>
</html>
