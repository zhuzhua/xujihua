<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn"  uri="http://java.sun.com/jsp/zxy/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<title>Insert title here</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
	</head>
	<body style="overflow: scroll; overflow-y: hidden">
		<div>


			<div id="main">
				<!--main_top-->
				<div class="main_top">
					<p>
						考试成绩
					</p>
				</div>
				<!--main_bottom-->

				<div class="main_bottom">

					<table width="100%" class="table" cellspacing="1"
						style="background: #BAC2CF; font-size: 14px;">
						<tr
							style="background: #F6F6F6; border: 0; text-align: center; line-height: 25px;">
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								编号
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								用户代码
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								用户名称
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								分数
							</td>
							<td
								style="color: #2B67D7; font-size: 14px; text-align: center; font-weight: bold;">
								时间
							</td>


						</tr>

				<c:forEach var="score" items="${scores }" varStatus="num">
				

						<tr
							style="background: #FFFFFF; border: 0; text-align: center; line-height: 25px;">
							<td style="color: #666666; font-size: 14px;">
								${num.count }
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${score.ucode } 
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${score.uname } 
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">
								${score.score } 
							</td>
							<td style="color: #666666; font-size: 14px; text-align: center;">

								${fn:subString(score.scoreTime,0,10)} 
							</td>
						</tr>


				</c:forEach>
						



					</table>
					<div>
						<script type="text/javascript">
							
						</script>



						<form name="spage"
							action="/user/examsRecords.do?command=showExamsRecords"
							method="post">
							<input type="button" value="首页" onclick="javascript:void(0)" disabled>
							<input type="button" value="上一页" onclick="javascript:void(0)" disabled>
							<input type="button" value="下一页" onclick="javascript:void(0)">
							<input type="button" value="尾页" onclick="javascript:void(0)">
							<input type="hidden" name="cp">
							<span style="color: red"> 1 / 2 </span> 跳转到：
							<select name="selPage" onchange="javascript:void(0)">

								<option value="1" selected>
									1
								</option>

								<option value="2">
									2
								</option>

							</select>
						</form>
					</div>


				</div>
			</div>


		</div>
		</div>
		</div>
	</body>
</html>
