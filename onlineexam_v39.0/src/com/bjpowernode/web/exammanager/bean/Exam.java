package com.bjpowernode.web.exammanager.bean;

public class Exam {
	private int   id             ;    
	private String   title          ;    
	private String   optionA        ;    
	private String   optionB        ;    
	private String   optionC        ;    
	private String   optionD        ;    
	private String   answer         ;    
	private String   registerTime   ;    
	private String   ts             ;
	
	
	
	
	public Exam() {
		super();
		// TODO Auto-generated constructor stub
	}




	public Exam(int id, String title, String optionA, String optionB,
			String optionC, String optionD, String answer, String registerTime,
			String ts) {
		super();
		this.id = id;
		this.title = title;
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
		this.answer = answer;
		this.registerTime = registerTime;
		this.ts = ts;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getTitle() {
		return title;
	}




	public void setTitle(String title) {
		this.title = title;
	}




	public String getOptionA() {
		return optionA;
	}




	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}




	public String getOptionB() {
		return optionB;
	}




	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}




	public String getOptionC() {
		return optionC;
	}




	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}




	public String getOptionD() {
		return optionD;
	}




	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}




	public String getAnswer() {
		return answer;
	}




	public void setAnswer(String answer) {
		this.answer = answer;
	}




	public String getRegisterTime() {
		return registerTime;
	}




	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}




	public String getTs() {
		return ts;
	}




	public void setTs(String ts) {
		this.ts = ts;
	}    
	
	
	
}
