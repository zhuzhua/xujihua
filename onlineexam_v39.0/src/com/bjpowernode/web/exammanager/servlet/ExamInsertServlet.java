package com.bjpowernode.web.exammanager.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.ExamDateFormat;
import com.bjpowernode.web.util.InitEntry;

public class ExamInsertServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//设置请求编码
//		request.setCharacterEncoding("UTF-8");
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			//获得请求数据
			Exam exam = new InitEntry<Exam>().getObj(request, new Exam());
			conn = DBUtil.getConnection();
			String sql = "insert into t_exam (title,optionA,optionB,optionC,optionD,answer,registerTime) values (?,?,?,?,?,?,?)";
			pstm = conn.prepareStatement(sql);
			//
			pstm.setString(1, exam.getTitle());
			pstm.setString(2, exam.getOptionA());
			pstm.setString(3, exam.getOptionB());
			pstm.setString(4, exam.getOptionC());
			pstm.setString(5, exam.getOptionD());
			pstm.setString(6, exam.getAnswer());
			pstm.setString(7, ExamDateFormat.getCurrentDate(Const.PATTERN_ALL));
			//
			pstm.executeUpdate();
			//重定向
			response.sendRedirect(request.getContextPath()+"/servlet/examQuery");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(null, pstm, conn);
		}
		
		
	}
}
