package com.bjpowernode.web.exammanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.exammanager.bean.Exam;

public class ExamEditUIServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获得需要修改数据的id
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		//连接数据库执行查询操作
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Exam exam  = null;
		try {
			conn = DBUtil.getConnection();
			String sql = "select title,optionA,optionB,optionC,optionD,answer from t_exam where id = ?";
			pstm = conn.prepareStatement(sql);
			//
			pstm.setInt(1, id);
			
			rs = pstm.executeQuery();
			
			if(rs.next()){
				String title = rs.getString("title");
				String optionA= rs.getString("optionA");
				String optionB= rs.getString("optionB");
				String optionC= rs.getString("optionC");
				String optionD= rs.getString("optionD");
				String answer= rs.getString("answer");
				
				//封装对象
				exam = new Exam(id, title, optionA, optionB, optionC, optionD, answer, null, null);
			}
			//把对象放入request作用域中
			request.setAttribute("exam", exam);
			
			//转发
			request.getRequestDispatcher("/exammanager/examEdit.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		
		
		
	}
}
