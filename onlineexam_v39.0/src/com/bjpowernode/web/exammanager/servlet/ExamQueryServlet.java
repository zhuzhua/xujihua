package com.bjpowernode.web.exammanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.PageModel;

public class ExamQueryServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PageModel<Exam> pageModel = new PageModel<Exam>();
		//当前页码
		int pageno = request.getParameter("pageno") == null?1:Integer.parseInt(request.getParameter("pageno"));
	
		pageModel.setPageno(pageno);
		
		//连接数据库执行查询操作
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<Exam> exams = new ArrayList<Exam>();
		try {
			conn = DBUtil.getConnection();
			
			String sql = "select count(id) totalRecord from t_exam";
			/*
			if(总记录数%每页显示记录数==0){
				总页数 = 总记录数/每页显示记录数
			}else {
				总页数 = 总记录数/每页显示记录数  +1;
			}
			总页数 = 总记录数%每页显示记录数==0 ? 总页数 = 总记录数/每页显示记录数 :总页数 = 总记录数/每页显示记录数  +1;
			*/
			
			pstm = conn.prepareStatement(sql);
			rs = pstm.executeQuery();
			int totalRecord = 0;
			/*if(rs.next()){
				totalRecord = rs.getInt("totalRecord");
			}*/
			totalRecord = rs.next()?rs.getInt("totalRecord"):0;
			
			
			pageModel.setTotalRecord(totalRecord);
			/*
			//总页数 = 总记录数%每页显示记录数==0 ?总记录数/每页显示记录数 : 总记录数/每页显示记录数  +1;
			totalPage = totalRecord%pageRecord==0&&totalRecord!=0?totalRecord/pageRecord:totalRecord/pageRecord+1;
			
			//如果当前页码大于总页数，已总页数为准（最后一页删除了，跳到上一页）
			
			pageno = pageno > totalPage ?totalPage:pageno;*/
			
			sql = "select id,title,optionA,optionB,optionC,optionD,answer from t_exam order by ts desc limit ?,? ";
			pstm = conn.prepareStatement(sql);
			//设置分页变量
			pstm.setInt(1,pageModel.beginRecord());
			pstm.setInt(2, pageModel.getPageRecord());
			
			rs = pstm.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String optionA = rs.getString("optionA");
				String optionB = rs.getString("optionB");
				String optionC = rs.getString("optionC");
				String optionD = rs.getString("optionD");
				String answer = rs.getString("answer");
				//封装对象         
				Exam exam = new Exam(id, title, optionA, optionB, optionC, optionD, answer, null, null);
				//把对象放入集合
				exams.add(exam);
			}
			pageModel.setModel(exams);
			//把pageModel放入request作用域中
			request.setAttribute("pageModel", pageModel);
			/*//把pageno放入request作用域中
			request.setAttribute("pageno", pageno);
			//把totalPage放入request作用域中
			request.setAttribute("totalPage", totalPage);
			//把集合放入request作用域中
			request.setAttribute("exams", exams);*/
			//转发
			request.getRequestDispatcher("/exammanager/examView.jsp").forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		
		
		
		
	}
}
