package com.bjpowernode.web.exammanager.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.InitEntry;

public class ExamEditServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//设置请求编码
//		request.setCharacterEncoding("UTF-8");
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			//获得请求数据
			Exam exam = new InitEntry<Exam>().getObj(request, new Exam());
			//连接数据库执行修改操作
			
			conn = DBUtil.getConnection();
			String sql = "update t_exam set title = ?,optionA=?,optionB=?,optionC=?,optionD=?,answer=? where id = ?";
			pstm = conn.prepareStatement(sql);
			
			pstm.setString(1, exam.getTitle());
			pstm.setString(2, exam.getOptionA());
			pstm.setString(3, exam.getOptionB());
			pstm.setString(4, exam.getOptionC());
			pstm.setString(5, exam.getOptionD());
			pstm.setString(6, exam.getAnswer());
			pstm.setInt(7, exam.getId());
			
			pstm.executeUpdate();
			
			response.sendRedirect(request.getContextPath()+"/servlet/examQuery");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(null, pstm, conn);
		}
		
		
	}
}
