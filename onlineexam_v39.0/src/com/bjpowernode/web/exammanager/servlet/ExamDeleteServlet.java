package com.bjpowernode.web.exammanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;

public class ExamDeleteServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//获得pageno
		String pageno = request.getParameter("pageno");
		//获得需要删除数据的id
		int id = Integer.parseInt(request.getParameter("id"));
		//连接数据库执行删除操作
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = DBUtil.getConnection();
			String sql = "delete from t_exam where id = ?";
			pstm = conn.prepareStatement(sql);
			
			pstm.setInt(1, id);
			
			pstm.executeUpdate();
			
			//重定向
			response.sendRedirect(request.getContextPath()+"/servlet/examQuery?pageno="+pageno);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(null, pstm, conn);
		}
		
		
	}
}
