package com.bjpowernode.web.user.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.usermanager.service.UserService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.InitEntry;

public class LoginServlet extends HttpServlet {
	private UserService userService = (UserService) BeanFactory.getBean("userService");
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			//获得用户登录信息
			User user = new InitEntry<User>().getObj(request, new User());
			//委托service处理业务
			boolean flag = userService.login(user);
			
			if(flag){//登录成功
				//获得session对象
				HttpSession session = request.getSession();
				//把用户放入session作用中
				session.setAttribute("user", user);
				//重定向到首页
				response.sendRedirect(request.getContextPath()+"/index.html");
			}else{//登录失败
				request.setAttribute(Const.ERRORINFO,"用户名称或者密码错误！");
				//转发login页面
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}
			
		}catch(Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			//重定向到错误页面
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}
		
		
		
	}
}
