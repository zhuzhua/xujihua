package com.bjpowernode.web.generics;

import com.bjpowernode.web.usermanager.bean.User;

public class TestGenerics {
	public static void main(String[] args) {
		//User user =  (User) new InitEntry().getObj(new User());
		//InitEntry<User> ie = new InitEntry<User>();
		User user =   new InitEntry<User>().getObj(new User());
	}
}
