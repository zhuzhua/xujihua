package com.bjpowernode.web.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		User user = new User();
		invokesetMethod(user,"age",23);
		invokeGetMethod(user,"age");
	}
	private static void invokesetMethod(User user, String field,Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		//uname -- > setUname  
		String methodName = "set"+field.toUpperCase().substring(0,1)+field.substring(1);
		Method [] methods = user.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if(method.getName().equals(methodName)){
				//获得字段的参数类型
				Class<?> clazz = method.getParameterTypes()[0];
				//根据形参类型，把实参类型转换为形参类型
				if(clazz.getSimpleName().equals("int")){
					method.invoke(user, Integer.parseInt(value.toString()));
				}else if(clazz.getSimpleName().equals("String")){
					method.invoke(user, value.toString());
				}
				//....
			}
		}
	}
	/**
	 * 
	 * @param obj
	 * @param field
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void invokeGetMethod(Object obj, String field) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		//uname -- > getUname  
		String methodName = "get"+field.toUpperCase().substring(0,1)+field.substring(1);
		//获得所有的方法对象【不包含继承下来的方法】
		Method [] methods = obj.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if(method.getName().equals(methodName)){
				/**
				 * obj : 方法所在的对象名称
				 * obj...: 方法参数列表
				 */
				Object str =  method.invoke(obj, null);
				System.out.println(str);
			}
		}
	}
}
