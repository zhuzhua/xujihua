package com.bjpowernode.web.reflect;

public class User {
	private int age;
	private String uname;
	public User(int age, String uname) {
		super();
		this.age = age;
		this.uname = uname;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	
}
