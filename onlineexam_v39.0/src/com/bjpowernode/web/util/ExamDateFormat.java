package com.bjpowernode.web.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExamDateFormat {
	public static String getCurrentDate(String pattern){
		Date date = new Date();
		//创建格式化对象并指定日期格式
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}
}
