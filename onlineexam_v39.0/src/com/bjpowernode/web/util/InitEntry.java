package com.bjpowernode.web.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.bjpowernode.web.usermanager.bean.User;

public class InitEntry<T> {
	
	public T getObj(HttpServletRequest request,T obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		//获得页面上所有的input对象的name属性值
		Enumeration<String> names = request.getParameterNames();
		while(names.hasMoreElements()){
			String name = names.nextElement();
			String value = request.getParameter(name);
			invokeMethod(obj,name,value);
		}
		
		return obj;
	}
	private  void invokeMethod(T obj, String field,Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		//uname -- > setUname  
		String methodName = "set"+field.toUpperCase().substring(0,1)+field.substring(1);
		Method [] methods = obj.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if(method.getName().equals(methodName)){
				//获得字段的参数类型
				Class<?> clazz = method.getParameterTypes()[0];
				//根据形参类型，把实参类型转换为形参类型
				if(clazz.getSimpleName().equals("int")){
					method.invoke(obj, Integer.parseInt(value.toString()));
				}else if(clazz.getSimpleName().equals("String")){
					method.invoke(obj, value.toString());
				}
				//....
			}
		}
	}
}
