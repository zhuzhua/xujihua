package com.bjpowernode.web.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import com.bjpowernode.web.exception.UserException;
import com.bjpowernode.web.usermanager.dao.UserDao;
import com.bjpowernode.web.usermanager.service.UserService;

public class BeanFactory {
	private static Map<String,Object> beans = new HashMap<String, Object>();
	
	public static void createBean(String filename){
		try {
			//1:导入支持库
			//2:创建文件解析对象
			SAXReader saxReader = new SAXReader();
			//把文件封装成字节输入流(文件必须在src下)
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
			//把流封装成document对象
			Document doc = saxReader.read(inputStream);
			
			String xpath = "/beans/bean";
			//获得所有的节点
			List<Node> nodes = doc.selectNodes(xpath);
			
			for (Node node : nodes) {
				//把节点转换为元素对象，便于取值
				Element ele = (Element)node;
				//取出属性的值
				String id = ele.attributeValue("id");
				//标签对中的字符文本
				String className = ele.getText();
				//把对象放入map集合
				beans.put(id, Class.forName(className).newInstance());
			}
			
			
			inputStream.close();
			
			
			/*beans.put("userDao", new UserDao());
			beans.put("userService", new UserService());*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//throw new UserException("文件读取失败！",e);
		}
		
	}
	/**
	 * 通过key获得value
	 * @param key
	 * @return
	 */
	public static Object getBean(String key){
		return beans.get(key);
	}
	
	public static void clear(){
		beans.clear();
		System.gc();
	}
}
