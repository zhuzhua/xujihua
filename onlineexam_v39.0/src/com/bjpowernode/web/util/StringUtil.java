package com.bjpowernode.web.util;

public class StringUtil {
	/**
	 * 注意：方法必须是静态的
	 * @param str
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	public static java.lang.String subString(java.lang.String str,int beginIndex ,int endIndex){
		return str.length() < endIndex?str:str.substring(beginIndex,endIndex);
	}
}
