package com.bjpowernode.web.util;

import java.util.List;

public class PageModel<T> {
	//当前页码
	private int pageno;
	//每页显示的记录数
	private int pageRecord = 3;
	//总页数
	private int totalPage = 0;
	//总记录数
	private int totalRecord;
	
	private List<T> model;
	
	public int getPageno() {
		return pageno > getTotalPage() ?getTotalPage():pageno;
	}
	public void setPageno(int pageno) {
		this.pageno = pageno;
	}
	public int getPageRecord() {
		return pageRecord;
	}
	public void setPageRecord(int pageRecord) {
		this.pageRecord = pageRecord;
	}
	//获取总页数
	public int getTotalPage() {
		return totalRecord%pageRecord==0&&totalRecord!=0?totalRecord/pageRecord:totalRecord/pageRecord+1;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	//起始记录
	public int beginRecord(){
		return (getPageno()-1)*pageRecord;
	}
	public List<T> getModel() {
		return model;
	}
	public void setModel(List<T> model) {
		this.model = model;
	}
	
	
}
