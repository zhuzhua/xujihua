package com.bjpowernode.web.util;

public class Const {
	public static final String PATTERN_ALL = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_YMD = "yyyy-MM-dd";
	public static final String ERRORINFO = "errorInfo";
	public static final int RANDRECORD = 3;
}
