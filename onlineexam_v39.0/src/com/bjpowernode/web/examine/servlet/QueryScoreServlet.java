package com.bjpowernode.web.examine.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.web.examine.exception.ExamineException;
import com.bjpowernode.web.examine.service.ExamineService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;

public class QueryScoreServlet extends HttpServlet{
	private ExamineService examineService = (ExamineService) BeanFactory.getBean("examineService");
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			
			List<Map<String,String>>  scores = examineService.queryScore();
			
			//把集合中的对象放入request作用域中
			request.setAttribute("scores", scores);
			
			//转发
			request.getRequestDispatcher("/examine/scoreView.jsp").forward(request, response);
			
		} catch (ExamineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败！":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}
		
		
	}
}
