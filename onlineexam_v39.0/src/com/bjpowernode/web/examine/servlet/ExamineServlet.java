package com.bjpowernode.web.examine.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.web.examine.exception.ExamineException;
import com.bjpowernode.web.examine.service.ExamineService;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;

/**
 * 
 * 控制器作用：
 * 	1.获取数据
 *  2.调用业务曾
 *  3.跳转
 * 
 * 	默认是单例的，在第一次访问时创建对象。
 * 
 *  Struts2框架通过Action程序代替Servlet处理请求，解决线程安全问题。Action是多例的。
 *  
 *  重点掌握：
 *  	三个范围：request(HttpServletRequest),session(HttpSession),application(ServletCotnext)
 *  	两种跳转方式：转发，重定向
 *  	生命周期方法：init(),service(),destroy()
 *  			一般重写doGet(),doPost()
 *  	常用接口：Servlet,ServletConfig，HttpServletRequest，HttpServletResponse
 *  			HttpSession，ServletCotnext，RequestDispatcher
 *  
 */
public class ExamineServlet extends HttpServlet {
	private ExamineService examineService = (ExamineService) BeanFactory.getBean("examineService");
	
	private int count =0 ;
	
	public synchronized void incrCount(){
		count++;
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		/*synchronized (response) {
			count++;
		}*/
		incrCount();		
		try {
			//委托service处理业务
			List<Exam> exams = examineService.examine();
			//把集合放入request作用域中
			request.setAttribute("exams", exams);
			//转发
			request.getRequestDispatcher("/examine/examine.jsp").forward(request, response);
		} catch (ExamineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
			
		}
		
	}
}
