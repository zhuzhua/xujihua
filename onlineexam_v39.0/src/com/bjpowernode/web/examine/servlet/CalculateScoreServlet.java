package com.bjpowernode.web.examine.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bjpowernode.web.examine.exception.ExamineException;
import com.bjpowernode.web.examine.service.ExamineService;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;

public class CalculateScoreServlet extends HttpServlet {
	private ExamineService examineService = (ExamineService) BeanFactory.getBean("examineService");
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			Map<String,String> id_answer = new HashMap<String, String>();
			//获得页面上的试题个数
			String size = request.getParameter("itemSize");
			
			for(int i = 1;i <=Integer.parseInt(size);i++){
				//获得id
				String id = request.getParameter("id"+i);
				//获得answer
				String answer = request.getParameter("option"+i);
				
				//把id和答案封装到map中
				id_answer.put(id, answer);
			}
			//获得计算的分数
			double score = examineService.calculateScore(id_answer);
			//获得session对象
			 HttpSession session = request.getSession(false);
			//获得当前登录用户的id
			int id= ((User) session.getAttribute("user")).getId();
			//委托service做分数业务的处理（插入）
			examineService.save(score,id);
			//重定向（/servlet/queryScore）
			response.sendRedirect(request.getContextPath()+"/servlet/queryScore");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败！":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		} 
	}
}
