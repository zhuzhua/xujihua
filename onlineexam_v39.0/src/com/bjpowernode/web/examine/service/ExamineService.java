package com.bjpowernode.web.examine.service;

import java.util.List;
import java.util.Map;

import com.bjpowernode.web.examine.dao.ExamineDao;
import com.bjpowernode.web.examine.exception.ExamineException;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.BeanFactory;

public class ExamineService {
	private ExamineDao  examineDao = (ExamineDao) BeanFactory.getBean("examineDao");

	public List<Exam> examine() throws ExamineException {
		// TODO Auto-generated method stub
		return examineDao.examine();
	}

	public double calculateScore(Map<String, String> id_answer) throws ExamineException {
		
		return examineDao.calculateScore(id_answer);
	}

	public void save(double score, int id) throws ExamineException {
		examineDao.insert(score,id);
		
	}

	public List<Map<String,String>> queryScore() throws ExamineException {
		
		return examineDao.queryScore();
	}
}
