package com.bjpowernode.web.examine.exception;

public class ExamineException extends Exception {

	public ExamineException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamineException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ExamineException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExamineException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
