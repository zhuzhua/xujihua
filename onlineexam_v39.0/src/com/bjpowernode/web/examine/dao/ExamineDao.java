package com.bjpowernode.web.examine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.examine.exception.ExamineException;
import com.bjpowernode.web.exammanager.bean.Exam;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.ExamDateFormat;

public class ExamineDao {

	public List<Exam> examine() throws ExamineException {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<Exam> exams = new ArrayList<Exam>();
		try {
			conn = DBUtil.getConnection();
			String sql = "select id,title,optionA,optionB,optionC,optionD from t_exam order by rand() limit ?";
			pstm = conn.prepareStatement(sql);
			
			pstm.setInt(1, Const.RANDRECORD);
			
			rs = pstm.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String optionA = rs.getString("optionA");
				String optionB = rs.getString("optionB");
				String optionC = rs.getString("optionC");
				String optionD = rs.getString("optionD");
				//封装对象
				Exam exam = new Exam(id, title, optionA, optionB, optionC, optionD, null, null, null);
				//把对象放入集合
				exams.add(exam);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new ExamineException("获得试卷失败！",e);
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		return exams;
	}
	
	public double calculateScore(Map<String, String> id_answer) throws ExamineException {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		double score = 0;
		try {
			conn = DBUtil.getConnection();
			String sql = "select id from t_exam where id = ? and answer = ?";
			pstm = conn.prepareStatement(sql);
			
			Set<Entry<String,String>> entrys = id_answer.entrySet();
			
			for (Entry<String, String> entry : entrys) {
				int id = Integer.parseInt(entry.getKey());
				String answer = entry.getValue();
				
				pstm.setInt(1, id);
				pstm.setString(2, answer);
				
				rs = pstm.executeQuery();
				if(rs.next()){//如果有值，说明回答正确
					score+=10;
				}
			}
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new ExamineException("计算分数失败！",e);
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		
		return score;
	}

	public void insert(double score, int id) throws ExamineException {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = DBUtil.getConnection();
			String sql = "insert into t_score (uid,score,scoreTime) values (?,?,?)";
			pstm = conn.prepareStatement(sql);
			
			//
			pstm.setInt(1, id);
			pstm.setDouble(2, score);
			pstm.setString(3, ExamDateFormat.getCurrentDate(Const.PATTERN_ALL));
			
			//
			pstm.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new ExamineException("插入分数记录失败！",e);
		}finally{
			DBUtil.close(null, pstm, conn);
		}
	}

	public List queryScore() throws ExamineException {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<Map<String,String>> scores = new ArrayList<Map<String,String>>();
		try {
			conn = DBUtil.getConnection();
			String sql = "select ucode,uname,score,scoreTime from t_user u join t_score s on u.id = s.uid order by scoreTime desc ";
			pstm = conn.prepareStatement(sql);
			//执行sql语句
			rs = pstm.executeQuery();
			
//			处理结果集
			while(rs.next()){
				
				String ucode = rs.getString("ucode");
				String uname = rs.getString("uname");
				String score = rs.getString("score");
				String scoreTime = rs.getString("scoreTime");
				
				//封装对象
				
				Map<String,String> mscore = new HashMap<String, String>();
				mscore.put("ucode", ucode);
				mscore.put("uname", uname);
				mscore.put("score", score);
				mscore.put("scoreTime", scoreTime);
				
				//把对象放入集合
				scores.add(mscore);
				
			}
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new ExamineException("查询分数失败！",e);
		}
		
		
		return scores;
	}

}



