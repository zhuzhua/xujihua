package com.bjpowernode.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bjpowernode.web.exception.UserException;
import com.bjpowernode.web.util.BeanFactory;
/**
 * 监听器:
 * 
 * 	监听的是ServletContext对象的创建和销毁。
 * 
 * 	ServletContext对象服务器启动时加载当前应用程序时被创建；
 * 	ServletContext对象在应用程序被卸载的销毁，或服务器停止时销毁。
 * 
 *  监听对象只被创建一次，服务器启动时，Web服务器加载应用程序，解析web.xml文件，读取监听器完整类名，加载类，获取字节码，反射创建。
 * 	单例的。
 */
public class InitBeanListener implements ServletContextListener{
	
	String filename;
	private static final String FILENAME = "application.xml";
	
	/**
	 * 当服务器启动的过程中，方法被调用。
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			
			ServletContext application = event.getServletContext();
			filename = application.getInitParameter("filename");
			if(filename==null){
				filename = FILENAME;
			}
			System.out.println(filename);
			BeanFactory.createBean(filename);
			
			//Class.forName("com.bjpowernode.web.util.BeanFactory");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		BeanFactory.clear();
	}

}
