package com.bjpowernode.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/**
 * 
 * 字符编码过滤器只能解决POST请求的中文乱码问题；
 * 如果是GET请求，中文乱码问题，需要通过Tomcat/conf/server.xml
 * <Connector port="80" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443"
			   URIEncoding="UTF-8"  //解决get请求乱码问题。
			   />
 	单例的，服务器启动时，反射创建。
 *
 */
public class FilterEncoding implements Filter{
	private String encoding;
	
	public FilterEncoding(){
		System.out.println("new FilterEncoding().......");
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		encoding = filterConfig.getInitParameter("encoding");
		if(encoding==null){
			encoding = "UTF-8";
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(encoding);
		response.setContentType("text/html;charset="+encoding);
		System.out.println(this.getClass().getName());
		chain.doFilter(request, response);//放过请求去目标资源（servlet,jsp,html.....）
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
