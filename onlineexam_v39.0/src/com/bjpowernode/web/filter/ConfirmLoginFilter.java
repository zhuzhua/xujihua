package com.bjpowernode.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ConfirmLoginFilter implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		//为了使用子类的方法，所有把ServletRequest转换为HttpServletRequest对象
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		
		//获得请求路径
		String path = request.getServletPath();
		
		//获得session'对象
		HttpSession session = request.getSession(false);
		//从session中取user对象
		if((session != null &&  session.getAttribute("user")!=null)  || "/login.jsp".equals(path) ||"/servlet/login".equals(path)){//如果登录过去目标资源
			chain.doFilter(request, response);//去目标资源
		}else{//如果没登录过去登录页面
			response.sendRedirect(request.getContextPath()+"/login.jsp");
		}
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
