package com.bjpowernode.web.usermanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.exception.UserException;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.util.InitEntry;
import com.bjpowernode.web.util.PageModel;
/**
 * 主要做数据库的一下操作如：CRUD
 * 
 * CRUD是指在做计算处理时的增加(Create)、读取(Retrieve)（重新得到数据）、更新(Update)和删除(Delete)几个单词的首字母简写。
 * @author Administrator
 *
 */
public class UserDao {

	public void insert(User user) throws UserException {
		Connection conn  = null;
		PreparedStatement pstm = null;
		
		try {
			//获得连接
			conn = DBUtil.getConnection();
			//创建数据库操作对象
			String sql = "insert into t_user (ucode,uname,pwd,email,telephone,registerTime) values (?,?,?,?,?,?)";
			pstm = conn.prepareStatement(sql);
			//为占位符号赋值
			pstm.setString(1, user.getUcode());
			pstm.setString(2, user.getUname());
			pstm.setString(3, user.getPwd());
			pstm.setString(4, user.getEmail());
			pstm.setString(5, user.getTelephone());
			pstm.setString(6, user.getRegisterTime());
			//执行语句
			pstm.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new UserException("添加用户信息失败！", e);
		}finally{
			DBUtil.close(null, pstm, conn);
		}
		
	}

	public void query(PageModel<User> pageModel) throws UserException {
		// TODO Auto-generated method stub
		//连接数据库
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<User>();
		
		try {
			conn = DBUtil.getConnection();
			/*
			if(总记录数%每页显示记录数==0)｛
				总页数 = 总记录数/每页显示记录数
			｝else{
				总页数 = 总记录数/每页显示记录数
			}*/
			
			String sql = "select count(id) as totalRecord from t_user ";
			
			pstm = conn.prepareStatement(sql);
			rs = pstm.executeQuery();
			/*
			while(rs.next()){
				int totalRecord = rs.getInt("totalRecord");
			}
			*/
			//总记录数
			int totalRecord = rs.next()?rs.getInt("totalRecord"):0;
			
			//把总记录数给分页模型
			pageModel.setTotalRecord(totalRecord);
			
			sql = "select id, ucode,uname,email,telephone,registerTime from t_user order by ts desc  limit ?,? ";
			pstm = conn.prepareStatement(sql);
			
			//设置分页变量
			pstm.setInt(1, pageModel.beginRecord());//起始记录
			pstm.setInt(2, pageModel.getPageRecord());
			
			rs = pstm.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt("id");
				String ucode = rs.getString("ucode");
				String uname = rs.getString("uname");
				String email = rs.getString("email");
				String telephone = rs.getString("telephone");
				String registerTime = rs.getString("registerTime");
				
				//封装对象
				User user = new User(id, ucode, uname, null, email, telephone, registerTime, null);
				
				//把对象放入集合
				
				users.add(user);
			}
			//把数据给分页模型
			pageModel.setModel(users);
			
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new UserException("查询用户信息失败！", e);
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
				
	}

	public void update(User user) throws UserException {
		// TODO Auto-generated method stub
		//连接数据库执行修改操作
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		
		try {
			
			conn = DBUtil.getConnection();
			String sql = "update t_user set ucode = ?,uname = ?,pwd = ?,email=?,telephone=? where id = ?";
			pstm = conn.prepareStatement(sql);
			//为占位符号赋值
			pstm.setString(1, user.getUcode());
			pstm.setString(2, user.getUname());
			pstm.setString(3, user.getPwd());
			pstm.setString(4, user.getEmail());
			pstm.setString(5, user.getTelephone());
			pstm.setInt(6, user.getId());
			//执行sql语句
			pstm.executeUpdate();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new UserException("修改用户信息失败！",e);
		}finally{
			DBUtil.close(null, pstm, conn);
		}
				
	}

	public User getUserById(String id) throws UserException {
		//连接数据库根据ID查询相应的记录
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		User user  = null;
		try {
			conn = DBUtil.getConnection();
			String sql = "select ucode,uname,pwd,email,telephone from t_user where id = ?";
			pstm = conn.prepareStatement(sql);
			pstm.setInt(1, Integer.parseInt(id));
			rs = pstm.executeQuery();
			if(rs.next()){
				String ucode = rs.getString("ucode");
				String uname = rs.getString("uname");
				String pwd = rs.getString("pwd");
				String email = rs.getString("email");
				String telephone = rs.getString("telephone");
				
				//封装对象
				user = new User(Integer.parseInt(id), ucode, uname, pwd, email, telephone, null, null);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new UserException("根据ID查询用户失败！", e);
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		return user;
	}

	public void deleteUserByIds(String[] ids) throws UserException {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			//获得连接
			conn = DBUtil.getConnection();
			//开启事务
			DBUtil.beginTransaction(conn);
			String sql = "delete from t_user where id = ?";
			//获得数据库操作对象
			pstm = conn.prepareStatement(sql);
			//int count = 0;
			for(String  id :ids){
				//为占位符赋值
				pstm.setInt(1, Integer.parseInt(id));
				//把需要执行的语句放入批处理缓存中
				pstm.addBatch();
				/*
				count ++;
				if(count % 100 ==0){
					pstm.executeBatch();//执行批处理语句
					pstm.clearBatch();//清空缓存
				}*/
			}
			pstm.executeBatch();//执行批处理语句
			//提交事务
			DBUtil.commit(conn);
			//重定向到查询
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			//回滚事务
			DBUtil.rollback(conn);
			throw new UserException("删除数据失败！",e);
		}finally{
			DBUtil.close(null, pstm, conn);
		}
	}

	/*public boolean login(User user) throws UserException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
			conn = DBUtil.getConnection();
			String sql = "select id from t_user where ucode = '"+user.getUcode()+"' and uname = '"+user.getUname()+"' and pwd = '"+user.getPwd()+"'";
			st = conn.createStatement();
			
			rs = st.executeQuery(sql);
			if(rs.next()){
				user.setId(rs.getInt("id"));
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new UserException("登录失败！",e);
		}finally{
			DBUtil.close(rs, st, conn);
		}
		return flag;
	}*/
	public boolean login(User user) throws UserException {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
			conn = DBUtil.getConnection();
			String sql = "select id from t_user where ucode = ? and uname = ? and pwd = ?";
			pstm = conn.prepareStatement(sql);
			
			pstm.setString(1, user.getUcode());
			pstm.setString(2, user.getUname());
			pstm.setString(3, user.getPwd());
			
			rs = pstm.executeQuery();
			if(rs.next()){
				user.setId(rs.getInt("id"));
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new UserException("登录失败！",e);
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		return flag;
	}

}
