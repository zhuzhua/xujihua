package com.bjpowernode.web.usermanager.bean;

public class User {
	private int   id              ;    
	private String   ucode           ;    
	private String   uname           ;    
	private String   pwd             ;    
	private String   email           ;    
	private String   telephone       ;    
	private String   registerTime    ;    
	private String   ts              ;
	
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public User(int id, String ucode, String uname, String pwd,
			String email, String telephone, String registerTime, String ts) {
		super();
		this.id = id;
		this.ucode = ucode;
		this.uname = uname;
		this.pwd = pwd;
		this.email = email;
		this.telephone = telephone;
		this.registerTime = registerTime;
		this.ts = ts;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUcode() {
		return ucode;
	}
	public void setUcode(String ucode) {
		this.ucode = ucode;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}  
	
	
	
	
}
