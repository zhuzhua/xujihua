package com.bjpowernode.web.usermanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.usermanager.service.UserService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.PageModel;

import sun.nio.cs.ext.DBCS_IBM_EBCDIC_Decoder;


public class UserQueryServlet extends HttpServlet{
	
	
	
	private UserService userService = (UserService) BeanFactory.getBean("userService");
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//创建分页模型对象
		PageModel<User> pageModel = new PageModel<User>();
		//获得页码(pageno)
		int pageno = request.getParameter("pageno")==null?1:Integer.parseInt(request.getParameter("pageno"));
		//把当前页码给分页模型
		pageModel.setPageno(pageno);
		
		
		try {
			
			//委托service处理业务
			
			userService.query(pageModel);
			
			//把pageModel放入request作用域中
			request.setAttribute("pageModel", pageModel);
			
			//转发
			request.getRequestDispatcher("/usermanager/userView.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}
		
		
	}
}
