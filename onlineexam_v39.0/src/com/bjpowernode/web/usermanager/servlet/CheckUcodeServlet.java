package com.bjpowernode.web.usermanager.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.util.Const;


public class CheckUcodeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获得ucode
		String ucode = request.getParameter("ucode");
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		
		try {
			conn = DBUtil.getConnection();
			String sql = "select id from t_user where ucode = ?";
			pstm = conn.prepareStatement(sql);
			pstm.setString(1, ucode);
			
			rs = pstm.executeQuery();
			
		/*	if(rs.next()){
				//不可以用
			}else{
				//可以使用
			}*/
			
			String info = rs.next()?"该用户代码不可以使用":"恭喜您，该用户代码可以使用";
			
//			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.print(info);
			out.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}finally{
			DBUtil.close(rs, pstm, conn);
		}
		
		
		
		
	}
}
