package com.bjpowernode.web.usermanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.usermanager.service.UserService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.InitEntry;

public class UserEditServlet extends HttpServlet {
	private UserService userService = (UserService) BeanFactory.getBean("userService");
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//设置请求数据的编码格式
//		request.setCharacterEncoding("UTF-8");
		
		
		
		
		try {
			
			User user = new InitEntry<User>().getObj(request, new User());
			
			//
			userService.Edit(user);
			
			//重定向去查询
			response.sendRedirect(request.getContextPath()+"/servlet/userQuery");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}
		
		
		
	}
}
