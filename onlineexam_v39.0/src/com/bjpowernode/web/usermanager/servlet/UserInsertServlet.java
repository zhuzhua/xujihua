package com.bjpowernode.web.usermanager.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.usermanager.service.UserService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;
import com.bjpowernode.web.util.ExamDateFormat;
import com.bjpowernode.web.util.InitEntry;

public class UserInsertServlet extends HttpServlet {
	
	private UserService userService = (UserService) BeanFactory.getBean("userService");
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//设置请求编码
//		request.setCharacterEncoding("UTF-8");
		try {
			//获取请求参数封装对象
			User user =  new InitEntry<User>().getObj(request, new User());
			
			String registerTime = ExamDateFormat.getCurrentDate(Const.PATTERN_ALL);
			user.setRegisterTime(registerTime);
			
			//委托service处理业务
			userService.save(user);
			
			//重定向到查询
			response.sendRedirect(request.getContextPath()+"/servlet/userQuery");
			
		} catch (Exception e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
		}
	}
	
	
}
