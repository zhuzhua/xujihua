package com.bjpowernode.web.usermanager.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.jdbc.util.DBUtil;
import com.bjpowernode.web.usermanager.service.UserService;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.Const;

public class UserDeleteServlet extends HttpServlet {
	private UserService userService = (UserService) BeanFactory.getBean("userService");
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获得当前页码
		
		String pageno = request.getParameter("pageno");
		
		//获得需要删除数据的ids
		String [] ids  = request.getParameterValues("childCBox");
		
	
		
		try {
			userService.deleteUserByIds(ids);
			//重定向到查询
			response.sendRedirect(request.getContextPath()+"/servlet/userQuery?pageno="+pageno);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.getSession().setAttribute(Const.ERRORINFO, e.getMessage()==null?"处理业务失败":e.getMessage());
			response.sendRedirect(request.getContextPath()+"/error.jsp");
			
		}
		
		
		
		
	}
}
