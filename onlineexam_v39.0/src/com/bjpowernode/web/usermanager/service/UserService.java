package com.bjpowernode.web.usermanager.service;

import com.bjpowernode.web.exception.UserException;
import com.bjpowernode.web.usermanager.bean.User;
import com.bjpowernode.web.usermanager.dao.UserDao;
import com.bjpowernode.web.util.BeanFactory;
import com.bjpowernode.web.util.PageModel;

public class UserService {
	private UserDao userDao = (UserDao) BeanFactory.getBean("userDao");
	public void save(User user) throws UserException {
		//ί��dao���ݲ���
		userDao.insert(user);
	}
	public void query(PageModel<User> pageModel) throws UserException {
		userDao.query(pageModel);
		
	}
	public void Edit(User user) throws UserException {
		userDao.update(user);
	}
	public User getUserById(String id) throws UserException {
		// TODO Auto-generated method stub
		return userDao.getUserById(id);
	}
	public void deleteUserByIds(String[] ids) throws UserException {
		userDao.deleteUserByIds(ids);
		
	}
	public boolean login(User user) throws UserException {
		return userDao.login(user);
	}

}
