package com.bjpowernode.jdbc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.ResourceBundle;

public class DBUtil {
	
	private static String driver ;
	private static String url    ;
	private static String uname  ;
	private static String pwd    ;
	
	static{
		
		try {
			//把资源文件封装成 ResourceBundle 类型，在内存中以key和value的形式存在
			ResourceBundle resourceBundle = ResourceBundle.getBundle("com.bjpowernode.jdbc.util.DBConfig");
			//通过key获得value
			driver = resourceBundle.getString("driver");
			url = resourceBundle.getString("url");
			uname = resourceBundle.getString("uname");
			pwd = resourceBundle.getString("pwd");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("读取配置文件失败！",e);
		}
		
	}
	
	//获得连接
	public static Connection getConnection (){
		Connection  conn  = null;
		try {
			//1:注册驱动
			Class.forName(driver);
			
			conn = DriverManager.getConnection(url,uname,pwd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("连接数据库失败！",e);
		}
		return conn;
	}
	
	//释放资源
	public static void close(ResultSet rs ,Statement pstm ,Connection conn){
		try{
			if (rs != null){
				rs.close();
			}
			
		}catch(SQLException e){
			e.printStackTrace();
			throw new RuntimeException("rs关闭失败！",e);
		}
		try{
			if (pstm != null){
				pstm.close();
			}
			
		}catch(SQLException e){
			e.printStackTrace();
			throw new RuntimeException("pstm关闭失败！",e);
		}
		try{
			if (conn != null){
				conn.close();
			}
			
		}catch(SQLException e){
			e.printStackTrace();
			throw new RuntimeException("conn关闭失败！",e);
		}
		
		
	}
	//开启事务
	public static void beginTransaction(Connection conn){
		try {
			if(conn != null){
				conn.setAutoCommit(false);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("开启事务失败！",e);
		}
	}
	//提交事务
	public static void commit(Connection conn){
		try {
			if(conn != null){
				conn.commit();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("提交事务失败！",e);
		}
	}
	
	//回滚事务
	public static void rollback(Connection conn){
		try {
			if(conn != null){
				conn.rollback();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("回滚事务失败！",e);
		}
	}
}
