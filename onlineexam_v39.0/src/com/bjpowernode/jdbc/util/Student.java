package com.bjpowernode.jdbc.util;
/**
 * 1:需要克隆的类必须实现  Cloneable 接口
 * 2：重写clone 方法
 * 
 * @author Administrator
 *
 */
public class Student implements Cloneable{
	private int age;
	private String sname;
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(int age, String sname) {
		super();
		this.age = age;
		this.sname = sname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	

	
	
	
}
